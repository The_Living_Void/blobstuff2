uniform sampler2D colormap;
uniform sampler2D pineapples;
uniform sampler2D bumpmap;
varying vec2  TexCoord;
uniform float red;
uniform float green;
uniform float blue;
uniform float dis;

void main(void) {
    float midColor;
//    float sinus = (sin( float(time))+1.0)*0.5;
    vec4 colors = texture2D(colormap, TexCoord);
    vec4 pine = texture2D(pineapples, TexCoord);
//    colors = smoothstep(0.2,0.3,colors);
    midColor = (colors.r + colors.g + colors.b)*0.3;
    float midPine = (colors.r + colors.g + colors.b)*0.3;
	gl_FragColor = vec4(colors.r, colors.g, colors.b, 1.0);
	gl_FragColor = vec4(1.0-(red-midColor), green, blue-midColor, midColor)*(1.0-dis) + vec4(red-midPine, 1.0-(green-midPine), midPine, 1.0) * dis;
//    gl_FragColor.a = 1.0;
//    midColor = (gl_FragColor.r + gl_FragColor.g + gl_FragColor.b)*0.3;
//    gl_FragColor = vec4(midColor);
//    gl_FragColor.g = midColor*TexCoord.x;
//    gl_FragColor.b = midColor*TexCoord.y;
}
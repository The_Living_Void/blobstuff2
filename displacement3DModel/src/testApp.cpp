#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    
	ofSetVerticalSync(true);
    
	ofDisableArbTex();
	ofEnableAlphaBlending();
    
//  ofShader
    shader.load("shaders/displace.vert", "shaders/displace.frag");
	
	colormap.loadImage("stips.jpg");
//	colormap.loadImage("pineapples/pineapple.jpg");
	bumpmap.loadImage("earth.jpg");
    
    // camera
    //initialize the video grabber
	vidGrabber.setVerbose(true);
	vidGrabber.initGrabber(1024,512);
    vidGrabber.listDevices();
	//store the width and height for convenience
	int width = vidGrabber.getWidth();
	int height = vidGrabber.getHeight();
    
    midColor = ofColor(0,0,0);
    
    // anti aliasing
    fbo.allocate(ofGetWidth(), ofGetHeight());
    texFbo.allocate(ofGetWidth(), ofGetHeight());
    squirrelFbo.allocate(ofGetWidth(), ofGetHeight());
    
    squirrelFbo.begin();
    ofBackground(255,255,255,0);
    squirrelFbo.end();
    
    
    // models
    torus.enableFlatColors();
    mobius.enableFlatColors();
    
    parabala.setup(-2, 2, -2, 2, .1, .1);
    torus.setup(0, 2*M_PI, 0,2*M_PI, .1, .1);
    mobius.setup(0, 2*M_PI, 0, 1, .1, .1);
    tube.setup(-2, 2, -2, 2,0,2,.1, .1);
    spriral.setup(0, 2*M_PI, .01);
    thing.setup(-10, 10, -10, 10, 1.0, 1.0);
    steiner.setup(-1, 1, -1, 1, .1, .1);
    boys.setup(-1, 1, -1, 1, .1, .1);
    breather.setup(-17, 17, -13, 13, 0.5, 0.5);
    
    ofSetBackgroundAuto(false);
	ofBackground(255,255,215);
    
    fbo.begin();
    ofBackground(0);
    fbo.end();
    
}

//--------------------------------------------------------------
void testApp::update(){
    
    thing.reload();
    steiner.reload();
    boys.reload();
    breather.reload();
    mesh = thing.getMesh();
    
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
    vidGrabber.update();
    
    texFbo.begin();
    float sinuss =sin(ofGetSystemTimeMicros()*0.000001);
    ofBackground(sinuss*25 + 25);
    ofPushStyle();
    ofSetColor(ofColor::white);
    ofCircle(texFbo.getWidth()/2, texFbo.getHeight()/2, sinuss*texFbo.getHeight()/4+texFbo.getHeight()/4);
    ofPopStyle();
    texFbo.end();
    
    
    ofImage pineapples;
    pineapples.loadImage("pineapples/pineapples.jpg");
    squirrelFbo.begin();
    ofClear(0,0,0,0);
    ofDisableArbTex();
	shader.begin();
	shader.setUniformTexture("colormap", colormap.getTextureReference(), 1);
	shader.setUniformTexture("bumpmap", vidGrabber.getTextureReference(), 2);
	shader.setUniformTexture("pineapples", pineapples.getTextureReference(), 3);
	shader.setUniform1f("maxHeight",1.7);
    int timeInt = ofGetSystemTimeMicros();
    float red = 0.5*sin(timeInt*0.0000001f)+0.5;
    float green = 0.5*sin(timeInt*0.000001f)+0.5;
    float blue = 0.5*cos(timeInt*0.00000011f)+0.5;
	shader.setUniform1f("red",red);
	shader.setUniform1f("green",green);
	shader.setUniform1f("blue",blue);
	shader.setUniform1f("dis",0.0);
    
    //    //lets tumble the world with the mouse, so push the matrix first alright
    glPushMatrix();
    float scale = ((float)mouseX/(float)ofGetWidth()) * 80.0;
    
    glTranslatef(ofGetWidth()/2,ofGetHeight()/2,0);
    
    glRotatef(-mouseY,1,0,0);
    glRotatef(mouseX+(timeInt%(360000000/2))*0.00005,0,1,0);
    glScalef(scale,scale,scale);
    glTranslatef(((mouseX/ofGetWidth()-0.5)*2.0), 0, 0);
//    ofEnableDepthTest();
    mesh.draw();
//    ofDisableDepthTest();

//    ofVboMesh mesh2 = ofSpherePrimitive(2, 16).getMesh();
//
	shader.setUniform1f("dis",1.0);
//    ofEnableDepthTest();
    //    mesh2.draw();
    boys.draw(true,true);
//    ofDisableDepthTest();
    
    glPopMatrix();
	shader.end();
    squirrelFbo.end();
    
    fbo.begin();
    ofEnableAlphaBlending();
    ofPushStyle();
//    ofSetColor(0,4);
//    ofRect(0,0,ofGetWidth(),ofGetHeight());
    ofPopStyle();
    ofPushMatrix();
    squirrelFbo.draw(0,0);
    ofPopMatrix();
    ofDisableAlphaBlending();
    fbo.end();
    
}

//--------------------------------------------------------------
void testApp::draw(){
//    ofPushStyle();
////    ofSetColor(255,255,255,10);
////    colormap.draw(0,0,squirrelFbo.getWidth(),squirrelFbo.getHeight());
//    ofSetColor(255,0,255,10);
//    ofRect(0,0,ofGetWidth(),ofGetHeight());
    //    ofPopStyle();
    
//    glClear(GL_DEPTH_BUFFER_BIT);
    //    glClear(GL_COLOR_BUFFER_BIT);
    int timeInt = ofGetSystemTimeMicros();

    ofClear(0,0,0);
//    float scaler = 2.0;
//    ofTranslate(fbo.getWidth()/2, fbo.getHeight()/2);
//    ofPushMatrix();
//    ofRotate(-0.1*((int)(timeInt*0.00001)%3600),0,0,1);
//    ofTranslate(-scaler*fbo.getWidth()/2, -scaler*fbo.getHeight()/2);
//    fbo.draw(0,0,fbo.getWidth()*scaler,fbo.getHeight()*scaler);
    fbo.draw(0,0,ofGetWidth(),ofGetHeight());
    
    glPushMatrix();
    float scale = ((float)mouseX/(float)ofGetWidth()) * 80.0;
    
    glTranslatef(ofGetWidth()/2,ofGetHeight()/2,0);
    glRotatef(-mouseY,1,0,0);
    glRotatef(mouseX+(timeInt%(360000000/2))*0.00005,0,1,0);
    glScalef(scale,scale,scale);
    glTranslatef(((mouseX/ofGetWidth()-0.5)*2.0), 0, 0);
    
    //draw in middle of the screen
    //tumble according to mouse
    //    glTranslatef(-ofGetWidth()/2/scale,-ofGetHeight()/2/scale,0);
    
    ofPushStyle();
    ofEnableDepthTest();
//    mesh.setColorForIndices(0, mesh.getIndices().size(), ofColor::white);
    ofSetColor(255);
    //    mesh.draw();
    mesh.enableColors();
    mesh.setColorForIndices(0, mesh.getIndices().size(), ofColor(0));
    ofSetLineWidth(1.0);
    ofNoFill();
    ofSetColor(0);
//    mesh.drawWireframe();
    
    ofDisableDepthTest();
    ofPopStyle();
    
    glPopMatrix();
    ofPopMatrix();
    
}
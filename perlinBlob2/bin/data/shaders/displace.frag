uniform sampler2D colormap;
uniform sampler2D bumpmap;
uniform sampler2D permTexture;          // Permutation texture
varying vec2 TexCoord;
varying vec4 pc;


//////////////////////////
// Phong Directional FS //
//////////////////////////

// -- Lighting varyings (from Vertex Shader)
varying vec3 norm, lightDir0, halfVector0;
varying vec4 diffuse0, ambient;

vec4 phongDir_FS()
{
    vec3 halfV;
    float NdotL, NdotHV;
    
    // The ambient term will always be present
    vec4 color = ambient;
    
    // compute the dot product between normal and ldir
    NdotL = max(dot(norm, lightDir0),0.0);
    
    if (NdotL > 0.0) {
        color += diffuse0 * NdotL;
        halfV = normalize(halfVector0);
        NdotHV = max(dot(norm, halfV), 0.0);
        color +=    gl_FrontMaterial.specular *
        gl_LightSource[0].specular *
        pow(NdotHV, gl_FrontMaterial.shininess);
    }
    return color;
}

void main(void) {
//    float midColor;
    
	vec4 tex = texture2D(colormap, TexCoord);
    vec4 pcc = vec4(1.0-0.3*pc.x, 0.3*pc.y, pc.z, 1.0);
    vec3 rgb = vec3(smoothstep(0.3,0.8,0.5*(tex.rgb + pcc.rgb)));
    vec4 final = smoothstep(0.3,0.8,0.5*(pcc + phongDir_FS() + vec4(rgb,1.0)));
    
    gl_FragColor = vec4(1.0-final.r,1.0-final.g,1.0-final.b,final.a);
//    midColor = (gl_FragColor.r + gl_FragColor.g + gl_FragColor.b)*0.3;
//    gl_FragColor.r = midColor;
//    gl_FragColor.g = midColor*TexCoord.x;
//    gl_FragColor.b = midColor*TexCoord.y;
}
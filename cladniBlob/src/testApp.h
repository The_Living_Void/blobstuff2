#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

class testApp : public ofSimpleApp{

	public:

		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased();


        float ex, ey;
        float easing;
        float deltaX;
        float deltaY;
        float zoom;
        int detail;

		int calcula(double x, double y, double xshift, double yshift, double m, double n, double zoom, double detail);
        int fc(int x, int y) ;
    
    ofFbo fbo;
};

#endif


//////////////////////////      uniforms: input from the application

uniform sampler2D colormap;
uniform sampler2D pineapples;
uniform sampler2D bumpmap;
uniform float maxHeight;
uniform float dis;

//////////////////////////      from here

varying vec2  TexCoord;

//////////////////////////      into the main void

void main(void) {

/////super//comment///////      believe it or not, gl_MultiTexCoord0, gl_ModelViewProjectionMatrix & gl_Vertex are just there.
    
//////////////////////////      TexCoord: the texture coordinates
    
	TexCoord = gl_MultiTexCoord0.st;
    
//////////////////////////      bumpColor: is the color of bumpmap at the point TexCoord
    
	vec4 bumpColor = texture2D(bumpmap, TexCoord);
    
//////////////////////////      df: is the intensity of the deformation, maximum is 1 (0.3 + 0.59 + 0.11)
    
	float df = 0.30*bumpColor.r + 0.59*bumpColor.g + 0.11*bumpColor.b;
    
//////////////////////////      newVertexPos: is calculated with a maximum of maxHeight
//////////////////////////      in the direction of the normal, with the original gl_Vertex as a basis
    
	vec4 newVertexPos = vec4(gl_Normal * df * maxHeight, 0.0) + gl_Vertex;

/////super//comment///////      gl_Position: the final position of our point/vertex!
    
//////////////////////////      this is kind of a if-else thing.
//////////////////////////      if dis == 0.0 -> take original gl_Vertex
//////////////////////////      else if dis == 1.0 -> newVertexPos
//////////////////////////      else interpolate, alright?
    
	gl_Position = (1.0-dis) * (gl_ModelViewProjectionMatrix * gl_Vertex) + dis * (gl_ModelViewProjectionMatrix * newVertexPos);
    
} 
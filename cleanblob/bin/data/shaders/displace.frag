//////////////////////////      pheeew so many comments to write.
//////////////////////////      uniforms: anyway, again input from application

uniform sampler2D colormap;
uniform sampler2D pineapples;
uniform sampler2D bumpmap;
uniform float red;
uniform float green;
uniform float blue;
uniform float dis;

//////////////////////////      from here

varying vec2  TexCoord;

//////////////////////////      into the main void

void main(void) {
    
//////////////////////////      colors: is the color of colormap at the point TexCoord
    
    vec4 colors = texture2D(colormap, TexCoord);
    
//////////////////////////      pine: is the color of pineapples at the point TexCoord
    
    vec4 pine = texture2D(pineapples, TexCoord);
    
//////////////////////////      smoothstep: in this case increases contrast
    
    colors = smoothstep(0.2,0.3,colors);
    pine = smoothstep(0.2,0.3,pine);
    
//////////////////////////      grayValues
    
    float grayValue = (colors.r + colors.g + colors.b)*0.3;
    float grayPine = (colors.r + colors.g + colors.b)*0.3;
    
/////super//comment///////      gl_FragColor is the final value of the "pixel"
    
//////////////////////////      just the texture
    
	gl_FragColor = vec4(colors.r, colors.g, colors.b, 1.0);
    
//////////////////////////      this is kind of a if-else thing.
//////////////////////////      if dis == 0.0 -> first vec4
//////////////////////////      else if dis == 1.0 -> second vec4
//////////////////////////      else interpolate, alright?
    
//	gl_FragColor =  vec4(1.0-(red-grayValue), green, blue-grayValue, grayValue)     *(1.0-dis) +
//                    vec4(red-grayPine, 1.0-(green-grayPine), grayPine, 1.0)         * dis;
    
//////////////////////////      make non-transparent no matter what
    
//    gl_FragColor.a = 1.0;
    
}

//..............................following code: dont-worry-yet stuff, aka to lazy to comment

#pragma once

#include "ofxMathMesh.h"
#include "ofxAnimatableFloat.h"
#include "MyEquations.h"

inline float sqr(float x)
{
    return x*x;
}

class Parabala:public ofx3dFunction {
public:
    Parabala():ofx3dFunction(){
        a.setCurve(EASE_IN_EASE_OUT);
        a.setRepeatType(LOOP_BACK_AND_FORTH);
        a.setDuration(3.0);
        a.animateFromTo(-.4, .4);
        ofAddListener(ofEvents().update, this, &Parabala::update);
    }
    
    float valueForPoint(float x,float z){
        return a.getCurrentValue() * pow(x, 2) + a.getCurrentValue() * pow(z, 2);
    }
    
    ofFloatColor colorForPoint(float x, float z,float y){
        float maxY = valueForPoint(getXMax(), getZMax());
        ofFloatColor color;
        float hueValue = ofMap(y, 0, maxY, 0, ofFloatColor::limit());
        color.setHsb(hueValue, 1.0, 1.0);
        return color;
    }
    
    void update(ofEventArgs &args){
        float dt = 1/ofGetFrameRate();
        a.update(dt);
    }

public:
    ofxAnimatableFloat a;
};


class Torus:public ofxParametricSurface{
public:
    Torus():ofxParametricSurface(){
        minU.setCurve(EASE_OUT);
        minU.setRepeatType(LOOP_BACK_AND_FORTH);
        minU.setDuration(4.0);
        minU.animateFromTo(2*M_PI,0);
        
        minV.setCurve(LINEAR);
        minV.setRepeatType(LOOP_BACK_AND_FORTH);
        minV.setDuration(4.0);
        minV.animateFromTo(2*M_PI,0);
        ofAddListener(ofEvents().update, this, &Torus::update);
    }
    ofPoint valueForPoint(float u,float v){
        float c = 2;
        float a = 1;
        float x = (c+ a* cos(v)) * cos(u);
        float y = a* sin(v);
        float z = (c+ a*cos(v)) *sin(u);
        return ofPoint(x,y,z);
    }
    
    ofVec2f texCoordForPoint(float u,float v,ofPoint value){
        float s = u/(2*M_PI);
        float t = v/(2*M_PI);
        return ofVec2f(s,t);
    }
    
    ofFloatColor backColorForPoint(float u,float v,ofPoint value){
        if (u <= M_PI/2) {
            return ofFloatColor::green;
        }else if(u <= M_PI ){
            return ofFloatColor::orange;
        }else if (u <= 3*M_PI/2){
            return ofFloatColor::pink;
        }else{
            return ofFloatColor::violet;
        }
    }
    
    void update(ofEventArgs &args){
        float dt = 1/ofGetFrameRate();
        minU.update(dt);
        minV.update(dt);
        setUMin(minU);
        setVMin(minV);
    }
    
public:
    ofxAnimatableFloat minU;
    ofxAnimatableFloat minV;
};


class Mobius:public ofxParametricSurface {

public:
    ofPoint valueForPoint(float u,float v){
        float a = 2.0;
        float x = (a + (v/2.0) * cos(u/2.0)) * cos(u);
        float y = (a + (v/2.0) * cos(u/2.0)) * sin(u);
        float z = (v/2.0) * sin(u/2.0);
        return ofPoint(x,y,z);
    }
    
    ofFloatColor colorForPoint(float u, float v, ofPoint value){
        if (u <= M_PI/2) {
            return ofFloatColor::red;
        }else if(u <= M_PI ){
            return ofFloatColor::green;
        }else if (u <= 3*M_PI/2){
            return ofFloatColor::white;
        }else{
            return ofFloatColor::yellow;
        }
    }
};

class Tube:public ofx3dFunction{
public:
    Tube():ofx3dFunction(){
        height.setCurve(LINEAR);
        height.setRepeatType(LOOP_BACK_AND_FORTH);
        height.setDuration(4.0);
        height.animateFromTo(2,4);
        ofAddListener(ofEvents().update, this, &Tube::update);
    }
    
    float valueForPoint(float x,float z){
        return 1.0/(5 * (pow(x, 2.0f) + pow(z, 2.0f)));
    }
    
    void update(ofEventArgs &args){
        float dt = 1/ofGetFrameRate();
        height.update(dt);
        setYMax(height);
    }
public:
    ofxAnimatableFloat height;
    
};

class Thing:public ofx3dFunction{
public:
    Thing():ofx3dFunction(){
        height.setCurve(LINEAR);
        height.setRepeatType(LOOP_BACK_AND_FORTH);
        height.setDuration(4.0);
        height.animateFromTo(1,40);
        ofAddListener(ofEvents().update, this, &Thing::update);
    }
    
    float valueForPoint(float x,float z){
//        return 1.0/(5 * (pow(x, 2.0f) + pow(z, 2.0f)));
//        z *= 0.1;
        return sin(height.getCurrentValue()*(pow(x*0.1f, 2.0f)+pow(z*0.1f,2.0f)))*1.0;
    }
    ofVec2f texCoordForPoint(float x,float z ,float y){
        return ofVec2f(0.5*x/getXMax() + 0.5, 0.5*z/getZMax() + 0.5);
    }
    
    void update(ofEventArgs &args){
        float dt = 1/ofGetFrameRate();
        height.update(dt);
        setYMax(height);
    }
public:
    ofxAnimatableFloat height;
    
};



class Breather:public ofxParametricSurface{
    
public:
    
    void setA(float _a){
        a = _a;
    }
    float getA(){
        return a;
    }
    
    void setUV(ofPoint _uv){
        uv = _uv;
    }
    ofPoint getUV(){
        return uv;
    }
    
    ofPoint valueForPoint(float u,float v){
        /*
         wsqr := 1 - aa * aa;
         w := sqrt(wsqr);
         denom := aa * (sqr(w * cosh(aa * u)) + sqr(aa * sin(w * v)))
         
         x = -u + (2 * wsqr * cosh(aa * u) * sinh(aa * u) / denom)
         y = 2 * w * cosh(aa * u) * (-(w * cos(v) * cos(w * v)) - (sin(v) * sin(w * v))) / denom
         z = 2 * w * cosh(aa * u) * (-(w * sin(v) * cos(w * v)) + (cos(v) * sin(w * v))) / denom
         
         r := 1 - b^2;
         w := Sqrt[r];
         denom := b*((w*Cosh[b*u])^2 + (b*Sin[w*v])^2)
         breather = {-u + (2*r*Cosh[b*u]*Sinh[b*u])/
         denom, (2*w*Cosh[b*u]*(-(w*Cos[v]*Cos[w*v]) - Sin[v]*Sin[w*v]))/
         denom, (2*w*Cosh[b*u]*(-(w*Sin[v]*Cos[w*v]) + Cos[v]*Sin[w*v]))/denom}
         
         ParametricPlot3D[
         Evaluate[breather /. b -> 0.4], {u, -13.2, 13.2}, {v, -37.4, 37.4},
         
         */
//        float a = abs(sin(ofGetSystemTimeMicros()*0.0000001)) * 0.4 + 0.7;
        float wsqr = 1.0 - a * a;
        float w = sqrt(abs(wsqr));
        float denom = a * (sqr(w * cosh(a * u)) + sqr(a * sin(w * v)));

        float x = -u + (2 * wsqr * cosh(a * u) * sinh(a * u) / denom);
        float y = 2 * w * cosh(a * u) * (-(w * cos(v) * cos(w * v)) - (sin(v) * sin(w * v))) / denom;
        float z = 2 * w * cosh(a * u) * (-(w * sin(v) * cos(w * v)) + (cos(v) * sin(w * v))) / denom;
//        cout << wsqr << " : " << w << " : " << denom << " : " << x << " : " << y << " : " << z << endl;
        return ofPoint(x,y,z);
    }
    
    ofVec2f texCoordForPoint(float u,float v,ofPoint value){
        //        return ofVec2f(u/getUMax(), v/getVMax());
        //        return ofVec2f(value.x*.1,value.y*.1);
        float x = ofMap(u,getUMin(),getUMax(),uv.x,uv.y);
        float y = ofMap(v,getVMin(),getVMax(),uv.x,uv.y);
        return ofVec2f(x,y);
    }
    
    
    ofVec2f backTexCoordForPoint(float u,float v,ofPoint value){
        //        return ofVec2f(u/getUMax(), v/getVMax());
        //        return ofVec2f(value.x*.1,value.y*.1);
        float x = ofMap(u,getUMin(),getUMax(),uv.y,uv.x); // turn around, it's the back
        float y = ofMap(v,getVMin(),getVMax(),uv.y,uv.x); // turn around, it's the back
        return ofVec2f(x,y);
    }
    ofFloatColor colorForPoint(float u, float v, ofPoint value){
        if (u <= M_PI/2) {
            return ofFloatColor::red;
        }else if(u <= M_PI ){
            return ofFloatColor::green;
        }else if (u <= 3*M_PI/2){
            return ofFloatColor::white;
        }else{
            return ofFloatColor::yellow;
        }
    }
private:
    float a;
    ofPoint uv;
};


class Boys:public ofxParametricSurface{
    
public:
    ofPoint valueForPoint(float u,float v){
        /*  
         P.x := (2/3)*(cos(u)*cos(2*v)+sqrt(2)*sin(u)*cos(v))*cos(u) /
         (sqrt(2) - sin(2*u)*sin(3*v));
         P.y := (2/3)*(cos(u)*sin(2*v)-sqrt(2)*sin(u)*sin(v))*cos(u) /
         (sqrt(2)-sin(2*u)*sin(3*v));
         P.z := sqrt(2)*cos(u)^2 / (sqrt(2) - sin(2*u)*sin(2*v));
         */
        float a = 2.0;
        float x = (a/3)*(cos(u)*cos(a*v)+sqrt(a)*sin(u)*cos(v))*cos(u) /
        (sqrt(a) - sin(a*u)*sin(3*v));
        float y = (a/3)*(cos(u)*sin(a*v)-sqrt(a)*sin(u)*sin(v))*cos(u) /
        (sqrt(a)-sin(a*u)*sin(3*v));
        float z = sqrt(a)*pow(cos(u), 2.0f) / (sqrt(a) - sin(a*u)*sin(a*v));
        return ofPoint(x,y,z);
    }
    
    ofVec2f texCoordForPoint(float u,float v,ofPoint value){
        //        return ofVec2f(u/getUMax(), v/getVMax());
//        return ofVec2f(value.x*.1,value.y*.1);
        float x = ofMap(u,getUMin(),getUMax(),0,1);
        float y = ofMap(v,getVMin(),getVMax(),0,1);
        return ofVec2f(x,y);
    }
    
    ofFloatColor colorForPoint(float u, float v, ofPoint value){
        if (u <= M_PI/2) {
            return ofFloatColor::red;
        }else if(u <= M_PI ){
            return ofFloatColor::green;
        }else if (u <= 3*M_PI/2){
            return ofFloatColor::white;
        }else{
            return ofFloatColor::yellow;
        }
    }
};


class Steiner:public ofxParametricSurface{
    
public:
    ofPoint valueForPoint(float u,float v){
        /*
         x = (aa * aa / 2) * (sin(2 * u) * cos(v) * cos(v))
         y = (aa * aa / 2) * (sin(u) * sin(2 * v))
         z = (aa * aa / 2) * (cos(u) * sin(2 * v))
         */
        float a = 2.0;
        float x = (a * a / 2) * (sin(2 * u) * cos(v) * cos(v));
        float y = (a * a / 2) * (sin(u) * sin(2 * v));
        float z = (a * a / 2) * (cos(u) * sin(2 * v));
        return ofPoint(x,y,z);
    }
    
    ofVec2f texCoordForPoint(float u,float v,ofPoint value){
//        return ofVec2f(u/getUMax(), v/getVMax());
//        return ofVec2f(value.x*.1,value.y*.1);
        float x = ofMap(u,getUMin(),getUMax(),0,1);
        float y = ofMap(v,getVMin(),getVMax(),0,1);
        return ofVec2f(x,y);
    }
    
    ofFloatColor colorForPoint(float u, float v, ofPoint value){
        if (u <= M_PI/2) {
            return ofFloatColor::red;
        }else if(u <= M_PI ){
            return ofFloatColor::green;
        }else if (u <= 3*M_PI/2){
            return ofFloatColor::white;
        }else{
            return ofFloatColor::yellow;
        }
    }
};


class Spiral:public ofxParametricCurve {
    
public:
    Spiral():ofxParametricCurve(){
        greenStart.setCurve(LINEAR);
        greenStart.setRepeatType(LOOP);
        greenStart.setDuration(40.0);
        greenStart.animateFromTo(0,2*M_PI);
        ofAddListener(ofEvents().update, this, &Spiral::update);
    }
    
    ofPoint valueForPoint(float t){
        float a = 1;
        float r = 1;
        float x = t*cos(6*t);
        float z = t*sin(6*t);
        float y = t;
        return ofPoint(x,y,z);
    }
    
    ofFloatColor colorForPoint(float t, ofPoint value){
        float colorRange = .1;
        float redStart = greenStart.getCurrentValue() + colorRange/2.0;
        float tWrapped = ofWrap(t, greenStart, (float)greenStart + colorRange);
        if (tWrapped >= (float)greenStart && tWrapped < redStart) {
            return ofFloatColor::green;
        }else{
            return ofFloatColor::red;
        }
        
    }
    void update(ofEventArgs &args){
        float dt = 1/ofGetFrameRate();
        greenStart.update(dt);
    }
public:
    ofxAnimatableFloat greenStart;
};









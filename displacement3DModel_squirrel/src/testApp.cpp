#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    
	ofSetVerticalSync(true);
    
    //load the squirrel model - the 3ds and the texture file need to be in the same folder
    squirrelModel.loadModel("squirrel/NewSquirrel.3ds", 20);
    
    //you can create as many rotations as you want
    //choose which axis you want it to effect
    //you can update these rotations later on
    squirrelModel.setRotation(0, 90, 1, 0, 0);
    squirrelModel.setRotation(1, 270, 0, 0, 1);
    squirrelModel.setScale(0.9, 0.9, 0.9);
    squirrelModel.setPosition(ofGetWidth()/2, ofGetHeight()/2, 0);
    
	ofDisableArbTex();
	ofEnableAlphaBlending();
    
//  ofShader
    shader.load("shaders/displace.vert", "shaders/displace.frag");
	
	colormap.loadImage("pineapples.jpg");
	bumpmap.loadImage("earth.jpg");
    
    // camera
    //initialize the video grabber
	vidGrabber.setVerbose(true);
	vidGrabber.initGrabber(1024,512);
    cout << " yeah yeah " << endl << endl;
    vidGrabber.listDevices();
    cout << " yeah 2 " << endl << endl;
	//store the width and height for convenience
	int width = vidGrabber.getWidth();
	int height = vidGrabber.getHeight();
    
    midColor = ofColor(0,0,0);
    
    // anti aliasing
    fbo.allocate(ofGetWidth(), ofGetHeight());
    texFbo.allocate(ofGetWidth(), ofGetHeight());
    squirrelFbo.allocate(ofGetWidth(), ofGetHeight());
    
    
    squirrelFbo.begin();
    ofBackground(255,255,255,0);
    squirrelFbo.end();
    
    ofSetBackgroundAuto(false);
	ofBackground(255,255,215);
    
}

//--------------------------------------------------------------
void testApp::update(){
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
    vidGrabber.update();
    
    texFbo.begin();
    float sinuss =sin(ofGetSystemTimeMicros()*0.000001);
    ofBackground(sinuss*25 + 25);
    ofPushStyle();
    ofSetColor(ofColor::white);
    ofCircle(texFbo.getWidth()/2, texFbo.getHeight()/2, sinuss*texFbo.getHeight()/4+texFbo.getHeight()/4);
    ofPopStyle();
    texFbo.end();
    
    
    
    squirrelFbo.begin();
    ofClear(0,0,0,0);
	shader.begin();
	shader.setUniformTexture("colormap", colormap.getTextureReference(), 1);
	shader.setUniformTexture("bumpmap", texFbo.getTextureReference(), 2);
	shader.setUniform1i("maxHeight",50);
    
    //    //lets tumble the world with the mouse, so push the matrix first alright
    glPushMatrix();
    float scale = ((float)mouseX/(float)ofGetWidth()) + 1.0;
    ofScale(scale,scale);
    
    //draw in middle of the screen
    glTranslatef(ofGetWidth()/2/scale,ofGetHeight()/2/scale,0);
    //tumble according to mouse
    glRotatef(-mouseY,1,0,0);
    glRotatef(mouseX,0,1,0);
    glTranslatef(-ofGetWidth()/2/scale,-ofGetHeight()/2/scale,0);
    
    ofSetColor(255, 255, 255, 255);
    ofEnableDepthTest();
    squirrelModel.draw();
    ofDisableDepthTest();
    
    glPopMatrix();
	shader.end();
    squirrelFbo.end();
    
    fbo.begin();
    ofEnableAlphaBlending();
    ofPushStyle();
    ofSetColor(255,0,255,1);
    ofRect(0,0,ofGetWidth(),ofGetHeight());
    ofPopStyle();
    ofPushMatrix();
    squirrelFbo.draw(0,0);
    ofPopMatrix();
    ofDisableAlphaBlending();
    fbo.end();
    
}

//--------------------------------------------------------------
void testApp::draw(){
//    ofPushStyle();
////    ofSetColor(255,255,255,10);
////    colormap.draw(0,0,squirrelFbo.getWidth(),squirrelFbo.getHeight());
//    ofSetColor(255,0,255,10);
//    ofRect(0,0,ofGetWidth(),ofGetHeight());
    //    ofPopStyle();
    
//    glClear(GL_DEPTH_BUFFER_BIT);
//    glClear(GL_COLOR_BUFFER_BIT);
    ofClear(0,0,0);
    fbo.draw(0,0,ofGetWidth(),ofGetHeight());
    
}
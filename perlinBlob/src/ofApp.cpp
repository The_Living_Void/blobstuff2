#include "ofApp.h"


float sin01(float in){
    float out = sin(in)*0.5+0.5;
    return out;
}

//--------------------------------------------------------------
void ofApp::setup(){
    shader.load("shaders/displace.vert", "shaders/displace.frag");
    permTexture.loadImage("util_img/permtexture.png");
    increment = 0.0;
    
    light.enable();
    light.setPointLight();
    light.setPosition(0,0,300);
    ofBackground(0);
    ofDisableArbTex();
}

//--------------------------------------------------------------
void ofApp::update(){

}

ofMesh makeGridMesh(float W, float H, float meshSize){
    ofMesh mesh;
    //Set up vertices
    for (int y=0; y<H; y++) {
        for (int x=0; x<W; x++) {
            mesh.addVertex(ofPoint((x - W/2) * meshSize, (y - H/2) * meshSize, 0 )); // adding texure coordinates allows us to bind textures to it later // --> this could be made into a function so that textures can be swapped / updated
            mesh.addTexCoord(ofPoint(x/W,y/H));//x * (videoWidth / W), y * (videoHeight / H)));
            mesh.addColor(ofColor(ofRandom(255.0), ofRandom(255.0), ofRandom(255.0)));
            mesh.addNormal(ofVec3f(ofRandom(1.0),ofRandom(1.0),ofRandom(1.0)));
        }
    }
    
    //Set up triangles' indices
    for (int y=0; y<H-1; y++) {
        for (int x=0; x<W-1; x++) {
            int i1 = x + W * y;
            int i2 = x+1 + W * y;
            int i3 = x + W * (y+1);
            int i4 = x+1 + W * (y+1);
            mesh.addTriangle( i1, i2, i3 );
            mesh.addTriangle( i2, i4, i3 );
        }
    }
    return mesh;
}

//--------------------------------------------------------------
void ofApp::draw(){
    increment += 1.1;
    /*
     
     uniform vec3 NoiseScale;    // Noise scale, 0.01 > 8
     uniform float Sharpness;    // Displacement 'sharpness', 0.1 > 5
     uniform float Displacement; // Displcement amount, 0 > 2
     uniform float Speed;        // Displacement rate, 0.01 > 1
     uniform float Timer;        // Feed incrementing value, infinite
     
     
     uniform vec2 PreScale, PreTranslate;    // Mesh pre-transform
     */
    
    shader.begin();
    shader.setUniformTexture("permTexture", permTexture.getTextureReference(), 0);
    shader.setUniform1f("BaseRadius", 200.0);
    
    shader.setUniform1f("NoiseScale", 4.0);
    shader.setUniform1f("Sharpness", 2.0);
    shader.setUniform1f("Displacement", 1.0);
    shader.setUniform1f("Speed", 0.5);
    shader.setUniform1f("Timer", increment);
    cout << increment << endl;
    
    shader.setUniform2f("PreScale", 1.0,1.0);
    shader.setUniform2f("PreTranslate", 0.0,0.0);
    ofBoxPrimitive box;
    box.set(ofGetHeight()/2);
    box.setResolution(56);
    
    ofSpherePrimitive sphere;
    sphere.set(ofGetHeight()/2, 20);
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    ofMesh mesh;
    mesh = makeGridMesh(20,20,20);
    
    ofRotate(increment,1,sin01(increment*0.01),0);
    sphere.draw();
    
//    box.draw();
    
    shader.end();
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}